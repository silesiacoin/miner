# Setup silesiacoin (Linux)
`sudo cp ./ssc /usr/local/bin/ssc`

`sudo chmod +x /usr/local/bin/ssc`

`ssc --datadir ssc init ./ssc.json`

# Start silesiacoin
## `--etherbase` is your silesiacoin address. If you have none visit silesiacoin.org

Please change to your wallet address here: `--etherbase 0x84dec2f0f58ae672c84d470e430b257cc8ddea20`

Command to run:

`ssc --datadir ssc --networkid 4004180 --etherbase 0x84dec2f0f58ae672c84d470e430b257cc8ddea20 --mine --rpc --rpccorsdomain localhost`


# GPU Mining
all contents for mining purposes is within `bin` directory

## Run ethminer (Linux)
`./bin/ethminer -G -P http://127.0.0.1:8545`

## Run PhoenixMiner (Windows)

Download: https://phoenixminer.org/

You can run PhoenixMiner on the Windows machine via this command:

Please change to your wallet address here: `-ewal 0x4bf025b0a964b6d022d5bb3a5bde54538f3587f4`

`.\PhoenixMiner.exe -epool http://127.0.0.1:8545 -ewal 0x4bf025b0a964b6d022d5bb3a5bde54538f3587f4 -epsw x -etha 0 -hstats 2 -rate 0`

## Windows

To run on Windows please use this branch: https://gitlab.com/silesiacoin/miner/-/tree/release/1.9-windows
